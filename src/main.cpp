#include "codegen.h"
#include "lexer.h"
#include "parser.h"
#include <fstream>
#include <iostream>
#include <memory>

int main(int argc, char **argv) {
  if (argc < 2) {
    std::cerr << "Usage : " << argv[0] << "<filename>" << std::endl;
    exit(EXIT_FAILURE);
  }

  std::ifstream file;
  file.open(argv[1]);
  auto lexer = Lexer(file);
  auto symtable = std::make_shared<Symtable>();
  auto codegen = std::make_unique<Codegen>(symtable, argv[1]);
  auto parser =
      Parser(std::make_unique<Lexer>(lexer), symtable, std::move(codegen));
  parser.mainLoop();
}