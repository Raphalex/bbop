#include "symtable.h"

Variable::Variable(bbop::Type type) : type(type), exists(true) {}

Variable::Variable() : type(bbop::i64) {}

Function::Function(std::vector<bbop::Type> proto, bbop::Type ret_type)
    : proto(std::move(proto)), ret_type(ret_type), exists(true) {}

Function::Function() : proto(std::vector<bbop::Type>()), ret_type(bbop::i64) {}
