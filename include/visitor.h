#ifndef __VISITOR_H__
#define __VISITOR_H__

/**
 * \file visitor.h
 * \brief Abstract visitor class to implement visitor pattern on the AST nodes
 */

#define VISIT(classname) virtual void visit(classname &node) = 0

class BlockAST;
class ReturnAST;
class DeclAST;
class IfElseAST;
class WhileAST;
class NumberExprAST;
class UnaryExprAST;
class BinaryExprAST;
class PrototypeAST;
class FunctionAST;
class CallExprAST;
class VariableExprAST;

/**
 * Abstract class used to implement visitor pattern.
 */
class Visitor {
public:
  /* A virtual function for each node type */
  VISIT(NumberExprAST);
  VISIT(UnaryExprAST);
  VISIT(BinaryExprAST);
  VISIT(PrototypeAST);
  VISIT(FunctionAST);
  VISIT(CallExprAST);
  VISIT(VariableExprAST);
  VISIT(BlockAST);
  VISIT(DeclAST);
  VISIT(ReturnAST);
  VISIT(IfElseAST);
  VISIT(WhileAST);
};

#endif