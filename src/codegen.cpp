#include "codegen.h"
#include "color.h"
#include "lexer.h"

#include <iostream>

Codegen::Codegen(std::shared_ptr<Symtable> symtable,
                 const std::string &module_name)
    : symtable(symtable) {
  this->llvmContext = std::make_unique<llvm::LLVMContext>();
  this->llvmModule =
      std::make_unique<llvm::Module>(module_name, *this->llvmContext);
  this->llvmBuilder = std::make_unique<llvm::IRBuilder<>>(*this->llvmContext);
}

void Codegen::visit(NumberExprAST &node) {
  // Check the type, and generate the corresponding IR
  switch (node.getType()) {
  case bbop::f64:
    this->llvmValue = llvm::ConstantFP::get(
        *this->llvmContext, llvm::APFloat(node.getFloatValue()));
    break;
  case bbop::i64:
    // TODO Adapt size of int
    this->llvmValue = llvm::ConstantInt::get(
        *this->llvmContext, llvm::APInt(64, node.getIntValue()));
    break;
  }
}

void Codegen::visit(BinaryExprAST &node) {
  /* Special case for the assignement operator */
  if (node.getOp() == Lexer::tok_assign) {
    // Check if the left hand side of the expression is a variable
    VariableExprAST *lhse = dynamic_cast<VariableExprAST *>(node.lhs.get());
    if (!lhse) {
      this->logError("destination of ':=' must be a variable");
      return;
    }

    // Generate code for right hand side
    node.rhs->accept(*this);

    llvm::Value *val = this->llvmValue;
    if (!val) {
      return;
    }

    // Get the adresse from the symbol table
    llvm::Value *var = this->symtable->variables[lhse->getName()].value;
    if (!var) {
      return this->logError("Unknown variable name");
    }

    this->llvmValue = this->llvmBuilder->CreateStore(val, var);

    return;
  }

  // Generate the code for each side of the expression
  node.lhs->accept(*this);
  llvm::Value *l = this->llvmValue;
  node.rhs->accept(*this);
  llvm::Value *r = this->llvmValue;

  if (!l || !r) {
    return;
  }

  // Generate the code that corresponds to each operation
  // TODO: Refactor all of this
  /* Idea : make a map (or smth) for each type, associate an integer,
     and make a function that generates the correct operations for a given
     type. If the type is a custom type (meta info somewhere), call a
     user-written function if it exists */
  if (node.getType() == bbop::i64) {
    switch (node.getOp()) {
    case '+':
      this->llvmValue = this->llvmBuilder->CreateAdd(l, r, "addtmp");
      break;
    case '-':
      this->llvmValue = this->llvmBuilder->CreateSub(l, r, "subtmp");
      break;
    case '*':
      this->llvmValue = this->llvmBuilder->CreateMul(l, r, "multmp");
      break;
    case '/':
      this->llvmValue = this->llvmBuilder->CreateSDiv(l, r, "divtmp");
      break;
    case '=':
      this->llvmValue = this->llvmBuilder->CreateICmpEQ(l, r, "eqtmp");
      break;
    case Lexer::tok_ne:
      this->llvmValue = this->llvmBuilder->CreateICmpNE(l, r, "netmp");
      break;
    case '>':
      this->llvmValue = this->llvmBuilder->CreateICmpSGT(l, r, "gttmp");
      break;
    case '<':
      this->llvmValue = this->llvmBuilder->CreateICmpSLT(l, r, "lttmp");
      break;
    case Lexer::tok_ge:
      this->llvmValue = this->llvmBuilder->CreateICmpSGE(l, r, "getmp");
      break;
    case Lexer::tok_le:
      this->llvmValue = this->llvmBuilder->CreateICmpSLE(l, r, "letmp");
      break;
    default:
      this->logError("invalid binary operator");
      break;
    }
  } else if (node.getType() == bbop::f64) {
    switch (node.getOp()) {
    case '+':
      this->llvmValue = this->llvmBuilder->CreateFAdd(l, r, "faddtmp");
      break;
    case '-':
      this->llvmValue = this->llvmBuilder->CreateFSub(l, r, "fsubtmp");
      break;
    case '*':
      this->llvmValue = this->llvmBuilder->CreateFMul(l, r, "fmultmp");
      break;
    case '/':
      this->llvmValue = this->llvmBuilder->CreateFDiv(l, r, "fdivtmp");
      break;
    default:
      this->logError("invalid binary operator");
      break;
    }
  } else {
    this->logError("unknown expression type");
  }
}

void Codegen::visit(PrototypeAST &node) {
  // Create a vector with the argument types of the function for llvm
  std::vector<llvm::Type *> arg_types;
  /* Fill the vector with llvm types (converted from bbop types with
     the btl::convert_type function
  */
  for (auto type : node.getArgTypes()) {
    arg_types.push_back(btl::convert_type(type, *this->llvmContext));
  }
  // Convert return type as well
  llvm::Type *ret_type =
      btl::convert_type(node.getRetType(), *this->llvmContext);

  // TODO (in a long time): handle variadics ?
  llvm::FunctionType *ft = llvm::FunctionType::get(ret_type, arg_types, false);

  // Create the llvm function (it gets stored in the module)
  llvm::Function *function =
      llvm::Function::Create(ft, llvm::Function::ExternalLinkage,
                             node.getName(), this->llvmModule.get());

  // Set names for arguments
  auto names = node.getArgNames();
  size_t id = 0;
  for (auto &arg : function->args()) {
    arg.setName(names[id++]);
  }

  this->llvmFunction = function;
}

void Codegen::visit(FunctionAST &node) {
  // Check if the function already exists
  auto proto = node.getProto();
  llvm::Function *function = this->llvmModule->getFunction(proto->getName());

  this->llvmFunction = function;

  auto types = proto->getArgTypes();

  // If it doesn't exist...
  if (!function) {
    // Generate code for the prototype
    proto->accept(*this);
    // Get the function from the accept (and this visit) call
    function = this->llvmFunction;
    if (!function) {
      return;
    }
  }

  /* If the function exists (for example if it's been declared before or if we
    just generated the prototype code from the if above), we check if it is
    empty, meaning there is no existing body for this function */
  if (!function->empty()) {
    this->logError("Redefinition of function " + proto->getName() +
                   " is not allowed");
  }

  /* We should now have an existing function with an empty body. We now have to
   * generate the code for the body */

  // Create a basic block
  llvm::BasicBlock *bb =
      llvm::BasicBlock::Create(*this->llvmContext, "entry", function);
  // Set the insert point for the builder
  this->llvmBuilder->SetInsertPoint(bb);

  // Counter for the argument types
  size_t i = 0;

  // Add the arguments to the symbol table
  for (llvm::Value &arg : function->args()) {
    // Create allocation
    llvm::AllocaInst *alloca =
        this->createAlloca(function, arg.getName().str(), types[i++]);
            (arg.getName(), types[i++]);

    // Store initial value in variable
    this->llvmBuilder->CreateStore(&arg, alloca);

    // Add allocation pointer in the symbol table
    this->symtable->variables[arg.getName().str()].value = alloca;
  }

  /* Before generating code for the body, we store the current function's return
     type. This allows the codegen function for return instructions to check the
     type.
   */
  this->cur_fun_ret_type = proto->getRetType();

  // Generate code for the body of the function
  node.getBody()->accept(*this);

  // Validate the code
  llvm::verifyFunction(*function, &llvm::errs());

  // TODO: Remove
  // Debug : dump IR
  function->print(llvm::errs());
}

void Codegen::visit(CallExprAST &node) {
  // Look up the function in the module table (and function symbol table)
  Function callee = this->symtable->functions[node.getCallee()];

  llvm::Function *llvmCallee = this->llvmModule->getFunction(node.getCallee());
  if (!callee.exists || !llvmCallee) {
    return this->logError("Function " + node.getCallee() + " doesn't exist");
  }

  std::vector<ExprASTPtr> args = node.getArgs();

  if (args.size() != callee.proto.size()) {
    return this->logError(
        "Incorrect number of arguments in call to function '" +
        node.getCallee() + "'");
  }

  // Vector of llvm values for the function arguments
  std::vector<llvm::Value *> argsV;

  // Keeps track of whether or not there are type errors in the arguments
  bool type_error = false;
  for (size_t i = 0; i < args.size(); ++i) {
    // Generate code for each expression inside the function call
    if (args[i]->getType() != callee.proto[i]) {
      type_error = true;
      this->logError("Argument " + std::to_string(i + 1) + " is of type '" +
                     bbop::type2str(args[i]->getType()) +
                     "' but is expected to be of type '" +
                     bbop::type2str(callee.proto[i]) + "'");
    }
    args[i]->accept(*this);
    argsV.push_back(this->llvmValue);
  }

  if (type_error) {
    return;
  }

  // Create the llvm function call instruction
  this->llvmValue = this->llvmBuilder->CreateCall(llvmCallee, argsV, "calltmp");
}

void Codegen::visit(VariableExprAST &node) {
  // Loop up the variable in the symbol table
  Variable var = this->symtable->variables[node.getName()];
  llvm::Value *value = var.value;
  llvm::Type *type = btl::convert_type(var.type, *this->llvmContext.get());
  if (!value) {
    this->logError("Unknown variable '" + node.getName() + "'");
  }
  this->llvmValue = this->llvmBuilder->CreateLoad(type, value, node.getName());
  this->llvmValue->print(llvm::errs());
  llvm::errs() << "\n";
}

void Codegen::visit(BlockAST &node) {
  auto instructions = node.getInstructions();

  // Generate code for each instruction
  for (auto &instr : instructions) {
    instr->accept(*this);
  }
}

void Codegen::visit(ReturnAST &node) {
  auto expr = node.getExpr();
  /* Before generating the code, check the type of
     the expression against the current function return type */
  if (expr->getType() != this->cur_fun_ret_type) {
    this->logError("Can't return expression of type " +
                   bbop::type2str(expr->getType()) + " in function returning " +
                   bbop::type2str(this->cur_fun_ret_type));
  }

  // Generate code for the expression
  expr->accept(*this);
  auto value = this->llvmValue;

  this->llvmBuilder->CreateRet(value);
}

void Codegen::visit(IfElseAST &node) {
  // Generate code for the condition
  ExprASTPtr cond = node.getCond();
  cond->accept(*this);

  InstrASTPtr then_instr, else_instr;
  then_instr = node.getThen();
  else_instr = node.getElse();

  llvm::Value *condV = this->llvmValue;

  /* Value that checks wether the then and else block break to the
     merge block or if they return on their own */
  bool blocks_break = false;

  // Error
  if (!condV) {
    return;
  }

  // Get current function
  llvm::Function *function = this->llvmBuilder->GetInsertBlock()->getParent();

  // Create then basic block
  llvm::BasicBlock *thenBB =
      llvm::BasicBlock::Create(*this->llvmContext, "then", function);

  // Create else basic block
  llvm::BasicBlock *elseBB =
      llvm::BasicBlock::Create(*this->llvmContext, "else");

  // Create merge block
  llvm::BasicBlock *mergeBB =
      llvm::BasicBlock::Create(*this->llvmContext, "ifcont");

  // Create conditionnal branch
  this->llvmBuilder->CreateCondBr(condV, thenBB, elseBB);

  // Change insert point to then block
  this->llvmBuilder->SetInsertPoint(thenBB);

  // Generate then code
  then_instr->accept(*this);

  // Codegen of the then can change insert block. Update thenBB
  thenBB = this->llvmBuilder->GetInsertBlock();

  // Create unconditionnal branch for the merge only if no terminator
  // was generated in the then code
  if (thenBB->getTerminator() == nullptr) {
    this->llvmBuilder->CreateBr(mergeBB);
    blocks_break = true;
  }

  // Emit else block (if it exists)
  function->insert(function->end(), elseBB);
  this->llvmBuilder->SetInsertPoint(elseBB);

  if (else_instr) {
    else_instr->accept(*this);
  }

  elseBB = this->llvmBuilder->GetInsertBlock();

  // Same thing as the then block
  if (elseBB->getTerminator() == nullptr) {
    this->llvmBuilder->CreateBr(mergeBB);
    blocks_break = true;
  }

  /* Emit merge block only if at least one of the blocks
     do break to the merge block */
  if (blocks_break) {
  function->insert(function->end(), mergeBB);
  }
  this->llvmBuilder->SetInsertPoint(mergeBB);
}

void Codegen::visit(UnaryExprAST &node) {
  auto operand = node.getOperand();
  // Generate code for the operand
  operand->accept(*this);
  llvm::Value *operandV = this->llvmValue;
  if (!operandV)
    return;

  // TODO: Change this
  if (operand->getType() == bbop::i64) {
    this->llvmValue = this->llvmBuilder->CreateSub(
        llvm::ConstantInt::get(*this->llvmContext, llvm::APInt(64, 0, true)),
        operandV, "uminus");
  }
  if (operand->getType() == bbop::f64) {
    this->llvmValue = this->llvmBuilder->CreateFSub(
        llvm::ConstantFP::get(*this->llvmContext, llvm::APFloat(0.)), operandV,
        "uminusF");
  }
}

void Codegen::visit(DeclAST &node) {
  auto names = node.getNames();
  auto expr = node.getInit();
  auto type = node.getType();

  // Loop through variable names and create allocs for each one
  size_t i = 0;
  for (auto var : names) {
    llvm::AllocaInst *alloca =
        this->createAlloca(this->llvmFunction, var, type);
    /* Check if the variable has an initialisation expression.
       if yes, generate the expression code dans the store instruction */
    if (expr[i] != nullptr) {
      expr[i++]->accept(*this);
      llvm::Value *init = this->llvmValue;

      // Create the store instruction
      this->llvmBuilder->CreateStore(init, alloca);
    }
    // Add the value in the symtable
    this->symtable->variables[var].value = alloca;
  }
}

void Codegen::visit(WhileAST &node) {
  // Put node attributes in local variables
  auto cond = node.getCond();
  auto instr = node.getInstr();

  // Generate code for the condition
  cond->accept(*this);

  llvm::Value *condV = this->llvmValue;

  // Get current function
  llvm::Function *function = this->llvmBuilder->GetInsertBlock()->getParent();

  // Create basic block
  llvm::BasicBlock *whileBB =
      llvm::BasicBlock::Create(*this->llvmContext, "while", function);

  // Create merge block
  llvm::BasicBlock *mergeBB =
      llvm::BasicBlock::Create(*this->llvmContext, "whilecont");

  // Create conditionnal branch
  this->llvmBuilder->CreateCondBr(condV, whileBB, mergeBB);

  // Set insert point
  this->llvmBuilder->SetInsertPoint(whileBB);

  // Generate while code
  instr->accept(*this);

  // Codegen of the while can change insert block, update it
  auto initialWhileBB = whileBB;
  whileBB = this->llvmBuilder->GetInsertBlock();

  // Generate condition code again
  cond->accept(*this);
  condV = this->llvmValue;

  this->llvmBuilder->CreateCondBr(condV, initialWhileBB, mergeBB);
  function->insert(function->end(), mergeBB);

  this->llvmBuilder->SetInsertPoint(mergeBB);
}

void Codegen::generateMachineCode() {
  // Don't generate code if the process encountered a fatal error
  if (this->fatal) {
    return;
  }
  llvm::InitializeAllTargetInfos();
  llvm::InitializeAllTargets();
  llvm::InitializeAllTargetMCs();
  llvm::InitializeAllAsmParsers();
  llvm::InitializeAllAsmPrinters();

  auto TargetTriple = llvm::sys::getDefaultTargetTriple();
  this->llvmModule->setTargetTriple(TargetTriple);

  std::string Error;
  auto Target = llvm::TargetRegistry::lookupTarget(TargetTriple, Error);

  // Print an error and exit if we couldn't find the requested target.
  // This generally occurs if we've forgotten to initialise the
  // TargetRegistry or we have a bogus target triple.
  if (!Target) {
    llvm::errs() << Error;
    exit(EXIT_FAILURE);
  }

  auto CPU = "generic";
  auto Features = "";

  llvm::TargetOptions opt;
  auto RM = std::optional<llvm::Reloc::Model>();
  auto TheTargetMachine =
      Target->createTargetMachine(TargetTriple, CPU, Features, opt, RM);

  this->llvmModule->setDataLayout(TheTargetMachine->createDataLayout());

  auto filename = "output.o";
  std::error_code EC;
  llvm::raw_fd_ostream dest(filename, EC, llvm::sys::fs::OpenFlags::OF_None);

  if (EC) {
    llvm::errs() << "Could not open file: " << EC.message();
    exit(EXIT_FAILURE);
  }

  llvm::legacy::PassManager pass;
  auto FileType = llvm::CGFT_ObjectFile;

  if (TheTargetMachine->addPassesToEmitFile(pass, dest, NULL, FileType)) {
    llvm::errs() << "TheTargetMachine can't emit a file of this type";
    exit(EXIT_FAILURE);
  }

  pass.run(*this->llvmModule);
  dest.flush();

  std::system((std::string("cc ") + std::string(filename)).c_str());
}

void Codegen::logError(const std::string &str) {
  this->fatal = true;
  std::cerr << color::red << "Error" << color::reset << ": " << str
            << std::endl;
}

void Codegen::logWarning(std::string &str) {
  std::cerr << color::red << "Warning" << color::reset << ": " << str
            << std::endl;
}

llvm::AllocaInst *Codegen::createAlloca(llvm::Function *function,
                                        const std::string &var_name,
                                        bbop::Type type) {
  /* Create a temporary llvm IRBuilder that points to the entry block of
  the current function. */
  llvm::IRBuilder<> tmb(&function->getEntryBlock(),
                        function->getEntryBlock().begin());

  /* Translate the type of the argument to an llvm Type */
  auto llvmType = btl::convert_type(type, *this->llvmContext);
  if (!llvmType) {
    // Error managment
    this->logError("Unkown type : " + var_name);
    return nullptr;
  }

  // Return the alloca *
  return tmb.CreateAlloca(llvmType, 0, var_name);
}
