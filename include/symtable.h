#ifndef __SYMTABLE_H__
#define __SYMTABLE_H__

/**
 * \file symtable.h
 * \brief Everything related to the symbol table
 */

#include "type.h"

#include "llvm/IR/IRBuilder.h"
#include <map>

#include <string>
#include <vector>

/**
 * Variable class (type and llvm value)
 */
class Variable {
public:
  // Type of the argument, defaults to i64
  bbop::Type type = bbop::i64;

  // Wether of not this is initialized
  bool exists = false;

  // AllocaInst pointer, that points to the argument
  llvm::AllocaInst *value = nullptr;

  /**
   * Constructor
   * \p type : type of the variable
   */
  Variable(bbop::Type type);

  /**
   * Default constructor
   */
  Variable();
};

/**
 * Function class (prototype and return type)
 */

class Function {
public:
  std::vector<bbop::Type> proto;
  bbop::Type ret_type;
  // Whether or not this function is initialized
  bool exists = false;

  /** Constructor
   * \p proto : vector of types, defining the types of the arguments
   * \p ret_type : return type
   */
  Function(std::vector<bbop::Type> proto, bbop::Type ret_type);

  /** Default constructor
   */
  Function();
};

// TODO: Far from finished
/**
 * Symbol table class
 */
class Symtable {
public:
  std::map<std::string, Variable> variables;
  std::map<std::string, Function> functions;
};

#endif
