#ifndef __CODEGEN_H__
#define __CODEGEN_H__

#include "ast.h"
#include "btl.h"
#include "symtable.h"
#include "visitor.h"

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Host.h"
#include "llvm/MC/TargetRegistry.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <memory>
#include <string>
#include <vector>

/**
 * \file codegen.h
 * \brief Contains the codegen class which generates code for each node.
 */

/**
 * \brief Code generation visitor
 */
class Codegen : public Visitor {
private:
  /* LLVM context, module, and builder */
  std::unique_ptr<llvm::LLVMContext> llvmContext;
  std::unique_ptr<llvm::Module> llvmModule;
  std::unique_ptr<llvm::IRBuilder<>> llvmBuilder;

  /* Indicates wether or not the code generation has encountered a fatal error
   */
  bool fatal = false;

  /** Symbol table */
  std::shared_ptr<Symtable> symtable;

  /** Simple error function */
  void logError(const std::string &str);

  /** Simple warning function */
  void logWarning(std::string &str);

  /** Member to hold different return values, since visit functions return void
   */
  llvm::Value *llvmValue = nullptr;

  /** Member to hold the last llvm::Function *object, since visit functions
   * return void. Same thing as the llvmValue member */
  llvm::Function *llvmFunction = nullptr;

  /**
   * The return type of the current function being generated
   */
  bbop::Type cur_fun_ret_type;

  /**
   * Creates an allocation for a variable of a given type in the entry block of
   * the current fonction. (used for mutable variables)
   */
  llvm::AllocaInst *createAlloca(llvm::Function *function,
                                 const std::string &var_name, bbop::Type type);

public:
  /** Constructor
   * \p symtable : a symbol table (in practice, shared with the parser)
   * \p module_name : the name of the module in which code is generated
   */
  Codegen(std::shared_ptr<Symtable> symtable, const std::string &module_name);

  /**
   * Code generation methods. Changes according to node type
   */
  void visit(NumberExprAST &node);

  void visit(UnaryExprAST &node);

  void visit(BinaryExprAST &node);

  void visit(PrototypeAST &node);

  void visit(FunctionAST &node);

  void visit(VariableExprAST &node);

  void visit(CallExprAST &node);

  void visit(BlockAST &node);

  void visit(DeclAST &node);

  void visit(ReturnAST &node);

  void visit(IfElseAST &node);

  void visit(WhileAST &node);

  // TODO: Temporary location
  /**
   * Generate executable
   */
  void generateMachineCode();
};

#endif
