#ifndef __COLOR_H__
#define __COLOR_H__

/**
 * \file color.h
 * \brief Simple macros for color output.
 */

#include <string>

namespace color {
static std::string reset = "\033[0m";

static std::string red = "\033[1;31m";

static std::string yellow = "\033[1;33m";

static std::string green = "\033[1;32m";
} // namespace color

#endif