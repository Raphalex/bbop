#include "parser.h"
#include "color.h"

#include <iostream>

Parser::Parser(std::unique_ptr<Lexer> lexer, std::shared_ptr<Symtable> symtable,
               std::unique_ptr<Codegen> codegen)
    : lexer(std::move(lexer)), symtable(symtable), codegen(std::move(codegen)) {
  /* Fill in the operator precedence map. Higher value means higher precedence
   */
  this->binopPrecedence[Lexer::tok_assign] = 2;
  this->binopPrecedence[Lexer::tok_ne] = 10;
  this->binopPrecedence[Lexer::tok_ge] = 10;
  this->binopPrecedence[Lexer::tok_le] = 10;
  this->binopPrecedence['='] = 10;
  this->binopPrecedence['<'] = 10;
  this->binopPrecedence['>'] = 10;
  this->binopPrecedence['+'] = 20;
  this->binopPrecedence['-'] = 20;
  this->binopPrecedence['*'] = 40;
  this->binopPrecedence['/'] = 40;
}

void Parser::mainLoop() {
  this->getNextToken();
  while (true) {
    switch (this->cur_tok) {
    case EOF:
      this->codegen->generateMachineCode();
      return;
    case Lexer::tok_fun:
      if (auto proto = this->parseDefinition()) {
        std::cerr << color::green << "Success" << color::reset
                  << ": parsed a function definition" << std::endl;
        proto->accept(*this->codegen);
      } else {
        this->getNextToken();
      }
      break;
    case Lexer::tok_extern:
      if (auto proto = this->parsePrototype()) {
        std::cerr << color::green << "Success" << color::reset
                  << ": parsed an extern" << std::endl;
        proto->accept(*this->codegen);
      } else {
        this->getNextToken();
      }
      break;
    default:
      this->logError<void *>("Expected function definition");
      return;
    }
  }
}

int Parser::getTokPrecedence() {
  /* If the token isn't ascii, it can't be an operator.
  Special case for the assign operator which is ':='.
  */
  if (!isascii(this->cur_tok) && this->cur_tok != Lexer::tok_assign) {
    return -1;
  }

  int tok_prec = binopPrecedence[this->cur_tok];
  if (tok_prec <= 0)
    return -1;
  return tok_prec;
}

template <typename T> T Parser::logError(const std::string &str) {
  std::cerr << "line :" << this->lexer->getLineNumber() << std::endl;
  std::cerr << color::red << "Error" << color::reset << ": " << str
            << std::endl;
  return nullptr;
}

void Parser::logWarning(const std::string &str) {
  std::cerr << "line :" << this->lexer->getLineNumber() << std::endl;
  std::cerr << color::yellow << "Warning" << color::reset << ": " << str
            << std::endl;
}

int Parser::getNextToken() { return this->cur_tok = this->lexer->getTok(); }

ExprASTPtr Parser::parsePrimary() {
  switch (this->cur_tok) {
  case Lexer::tok_int:
    return this->parseIntExpr();
  case Lexer::tok_float:
    return this->parseFloatExpr();
  case '(':
    return this->parseParenExpr();
  case Lexer::tok_id:
    return this->parseIdExpr();
  default:
    return this->logError<ExprASTPtr>(
        "Unknown token when expecting an expression");
  }
}

ExprASTPtr Parser::parseUnary() {
  // If this isn't a unary expression, it must be a primary expression
  // TODO: Add other unary operators
  if (this->cur_tok != '-')
    return parsePrimary();

  char op = this->cur_tok;
  // Eat operator
  this->getNextToken();
  if (auto operand = parseUnary())
    return std::make_unique<UnaryExprAST>(op, std::move(operand));
  return nullptr;
}

InstrASTPtr Parser::parseInstr() {
  /* Depending on the current token, we can have multiple instructions */
  InstrASTPtr result;
  /* If a ';' is needed after the instruction, store in the 'result' variable.
     Otherwise simply return the result */
  switch (this->cur_tok) {
  // Beginning of a block
  case '{':
    return this->parseBlock();
  // Return expression
  case Lexer::tok_return:
    result = this->parseReturn();
    break;
  // If else
  case Lexer::tok_if:
    return this->parseIfElse();
  // while
  case Lexer::tok_while:
    return this->parseWhile();
  case Lexer::tok_decl:
    result = this->parseDecl();
    break;
  default:
    result = this->parseExpr();
    break;
  }

  if (this->cur_tok != ';') {
    return logError<InstrASTPtr>("Expected ';' after instruction");
  }

  // Eat ';'
  this->getNextToken();

  return result;
}

InstrASTPtr Parser::parseBlock() {
  /* We should be called on a '{' */
  // Eat the '{'
  this->getNextToken();

  // We are going to store each instruction in this vector
  std::vector<InstrASTPtr> instructions;
  // We now parse expressions while the block isn't closed
  while (this->cur_tok != '}') {
    // Parse instruction and add it to our vector
    auto instr = this->parseInstr();
    if (!instr) {
      return nullptr;
    }
    instructions.push_back(std::move(instr));
  }

  // Eat '}'
  this->getNextToken();

  return std::make_unique<BlockAST>(std::move(instructions));
}

InstrASTPtr Parser::parseReturn() {
  // Should be called when on a 'return' keyword
  // Eat return
  this->getNextToken();

  // Parse expression
  ExprASTPtr expr = this->parseExpr();

  // Error case
  if (!expr) {
    return nullptr;
  }

  return std::make_unique<ReturnAST>(std::move(expr));
}

InstrASTPtr Parser::parseIfElse() {
  /* Assume this function is called on an 'if' token */
  // Eat 'if'
  this->getNextToken();

  // Parse condition
  ExprASTPtr cond = this->parseExpr();

  if (!cond) {
    return nullptr;
  }

  // Parse then block
  InstrASTPtr then_instr = this->parseInstr();
  // Propagate error
  if (!then_instr) {
    return nullptr;
  }

  // Case where there is no else
  if (this->cur_tok != Lexer::tok_else) {
    return std::make_unique<IfElseAST>(std::move(cond), std::move(then_instr),
                                       nullptr);
  }

  // Eat 'else"
  this->getNextToken();

  // Parse else block
  InstrASTPtr else_instr = this->parseInstr();

  // Propagate error
  if (!else_instr) {
    return nullptr;
  }

  return std::make_unique<IfElseAST>(std::move(cond), std::move(then_instr),
                                     std::move(else_instr));
}

InstrASTPtr Parser::parseWhile() {
  // This function is supposed to be called on an 'while' token
  // Eat 'while'
  this->getNextToken();

  // Parse condition
  auto cond = this->parseExpr();
  if (!cond) {
    return nullptr;
  }

  // Parse instruction
  auto instr = this->parseInstr();
  if (!instr) {
    return nullptr;
  }

  return std::make_unique<WhileAST>(std::move(cond), std::move(instr));
}

InstrASTPtr Parser::parseDecl() {
  // This function is supposed to be called on a 'decl' token
  // Eat 'decl'
  this->getNextToken();

  // This helps us keep track of wether we've found the current type for the
  // variables or not
  bool determined_type = false;
  bbop::Type type;
  std::vector<std::string> names;
  std::vector<ExprASTPtr> init;

  while (true) {
    if (this->cur_tok != Lexer::tok_id) {
      return this->logError<InstrASTPtr>("Expected identifier");
    }
    // Add identifier to list of names
    names.push_back(this->lexer->getIdStr());
    // Eat identifier
    this->getNextToken();
    // Case where the variable is initialized
    if (this->cur_tok == Lexer::tok_assign) {
      // Eat ':='
      this->getNextToken();
      // parse initialisation expression
      if (auto expr = this->parseExpr()) {
        // Add it to the list if it's parsed correctly...
        if (expr->getType() != type && determined_type) {
          return this->logError<InstrASTPtr>(
              "Initialisation expressions have different types");
        }
        if (!determined_type) {
          type = expr->getType();
          determined_type = true;
        }
        init.push_back(std::move(expr));
      } else {
        // ... propagate the error if it isn't
        return nullptr;
      }
    }
    // Add a nullptr to the init vector to show that this variable isn't
    // initialized.
    init.push_back(nullptr);

    // Case where the type is finally specified
    if (this->cur_tok == ':') {
      // Eat ':'
      this->getNextToken();
      if (this->cur_tok != Lexer::tok_type) {
        return this->logError<InstrASTPtr>("Expected type specifier");
      }
      // Check type coherence
      if (this->lexer->getType() != type && determined_type) {
        return this->logError<InstrASTPtr>(
            "Specified type doesn't match type of initialisation expressions");
      }
      if (!determined_type) {
        type = this->lexer->getType();
      }
      // Eat type specifier
      this->getNextToken();
      break;
    }
    if (this->cur_tok != ',') {
      return logError<InstrASTPtr>("Expected ',' or ':'");
    }
    this->getNextToken();
  }

  // Fill symtable with variable names
  for (size_t i = 0; i < names.size(); ++i) {
    // Check if the variable already exists
    if (this->symtable->variables[names[i]].exists) {
      return this->logError<InstrASTPtr>("Redeclaration of variable " +
                                         names[i]);
    }
    this->symtable->variables[names[i]] = Variable(type);
  }
  return std::make_unique<DeclAST>(type, std::move(names), std::move(init));
}

ExprASTPtr Parser::parseIntExpr() {
  /* Create a Number node with the integer constructor */
  auto result = std::make_unique<NumberExprAST>(this->lexer->getIntNumValue());
  /* Eat the number */
  this->getNextToken();
  return std::move(result);
}

ExprASTPtr Parser::parseFloatExpr() {
  /* Create a Number node with the float constructor */
  auto result =
      std::make_unique<NumberExprAST>(this->lexer->getFloatNumValue());
  /* Eat the number */
  this->getNextToken();
  return std::move(result);
}

ExprASTPtr Parser::parseParenExpr() {
  // Eat the '('
  this->getNextToken();
  // Parse th expression inside the parenthesis
  auto result = this->parseExpr();
  // If there is a problem while parsing, we propagate the error
  if (!result) {
    return nullptr;
  }
  // Check if the parenthesis closes correctly
  if (this->cur_tok != ')') {
    return this->logError<ExprASTPtr>("Expected ')'");
  }
  // Eat ')'
  this->getNextToken();
  return result;
}

ExprASTPtr Parser::parseBinOpRHS(int expr_prec, ExprASTPtr lhs) {
  while (true) {
    // Get current operator precedence (if it is an operator)
    int tok_prec = this->getTokPrecedence();

    /* If the current token has lower precedence than the current expression
      being parsed (or if it isn't an operator, since that returns -1), we
      stop. */
    if (tok_prec < expr_prec) {
      return lhs;
    }

    /* Otherwise, we know this is a binop */
    int binop = this->cur_tok;
    this->getNextToken(); // Eat operator

    auto rhs = this->parseUnary();
    if (!rhs) {
      return nullptr;
    }

    // Check the precedence of the operator after the right hand side
    int next_prec = this->getTokPrecedence();

    /* If the precedence is higher, we create a new binary expression with the
      whole expression after our operator as its lhs */
    if (tok_prec < next_prec) {
      rhs = this->parseBinOpRHS(tok_prec + 1, std::move(rhs));
      if (rhs->getType() != lhs->getType()) {
        return this->logError<ExprASTPtr>("Binary expression type mismatch");
      }
      if (!rhs) {
        return nullptr;
      }
    }
    // In any case, we merge everything together.
    if (lhs->getType() != rhs->getType()) {
      return this->logError<ExprASTPtr>("Binary expression type mismatch");
    }
    lhs = std::make_unique<BinaryExprAST>(binop, std::move(lhs), std::move(rhs),
                                          lhs->getType());
  }
}

ExprASTPtr Parser::parseExpr() {
  auto lhs = this->parseUnary();
  if (!lhs) {
    return nullptr;
  }
  return parseBinOpRHS(0, std::move(lhs));
}

ExprASTPtr Parser::parseIdExpr() {
  // This should be called when the token is the identifier
  std::string name = this->lexer->getIdStr();

  // Eat id
  this->getNextToken();

  // Simple variable reference
  if (this->cur_tok != '(') {
    // Check if there exists a variable with this name in the symbol table
    if (this->symtable->variables.find(name) ==
        this->symtable->variables.end()) {
      return this->logError<ExprASTPtr>("undeclared variable '" + name + "'");
    }
    // If it exists, fetch the type
    bbop::Type type = this->symtable->variables[name].type;
    return std::make_unique<VariableExprAST>(name, type);
  }

  // Function call
  // Eat '('
  this->getNextToken();
  std::vector<ExprASTPtr> args;
  if (this->cur_tok != ')') {
    while (true) {
      // Parse the expressions in the argument list
      if (auto arg = this->parseExpr()) {
        args.push_back(std::move(arg));
      } else {
        return nullptr;
      }

      // IF we find a ')', this is the end of the argument list
      if (this->cur_tok == ')') {
        break;
      }

      // Separate each argument with ','
      if (this->cur_tok != ',') {
        return this->logError<ExprASTPtr>(
            "Expected ')' or ',' in argument list");
      }
      this->getNextToken();
    }
  }
  // Eat ')'
  this->getNextToken();

  // Fetch return type from symbol table
  bbop::Type ret_type = this->symtable->functions[name].ret_type;
  return std::make_unique<CallExprAST>(name, std::move(args), ret_type);
}

PrototypeASTPtr Parser::parsePrototype() {
  /* We assume this function was called on a "fun" or "extern" keyword,
     so we eat it right away */
  this->getNextToken();

  // Now we expect an identifier
  if (this->cur_tok != Lexer::tok_id) {
    return this->logError<PrototypeASTPtr>(
        "Expected identifier after 'fun' keyword");
  }
  // Eat identifier and store the function name
  std::string name = this->lexer->getIdStr();
  this->getNextToken();

  if (this->cur_tok != '(') {
    return this->logError<PrototypeASTPtr>("Expected '(' after function name");
  }
  // Eat '('
  this->getNextToken();

  // arguments vector
  std::vector<std::pair<bbop::Type, std::string>> args;

  // arguments name vector
  std::vector<std::string> arg_names;
  // arguments types vector
  std::vector<bbop::Type> arg_types;

  while (true) {
    // Case where the argument list is finished
    if (this->cur_tok == ')') {
      // Eat ')'
      this->getNextToken();
      break;
    }

    size_t start_index = args.size();
    do {
      // Get argument name
      if (this->cur_tok != Lexer::tok_id) {
        return this->logError<PrototypeASTPtr>("Expected identifier");
      }
      // Store argument name in vector
      arg_names.push_back(this->lexer->getIdStr());
      // Eat argument name
      this->getNextToken();
      // '||' used to eat the ',' only
    } while (this->cur_tok == ',' && this->getNextToken());

    // Check for the ':'
    if (this->cur_tok != ':') {
      return this->logError<PrototypeASTPtr>(
          "Expected ':' after argument name");
    }
    // Eat ':'
    this->getNextToken();

    if (this->cur_tok != Lexer::tok_type) {
      return this->logError<PrototypeASTPtr>(
          "Expected type specifier after ':'");
    }

    while (arg_types.size() != arg_names.size()) {
      arg_types.push_back(this->lexer->getType());
    }

    this->getNextToken();

    /* Either there is another argument (',') or it's the end
       of the argument list */
    if (this->cur_tok != ',' && this->cur_tok != ')') {
      return this->logError<PrototypeASTPtr>("Expected ','");
    }

    if (this->cur_tok == ',') {
      // If we find a ',', eat it
      this->getNextToken();
    }
  }
  // TODO Handle procedures (function with no return value)

  // Fill the args vector
  for (size_t i = 0; i < arg_names.size(); ++i) {
    args.push_back(std::make_pair(arg_types[i], arg_names[i]));
  }

  if (this->cur_tok != ':') {
    return this->logError<PrototypeASTPtr>("Expected ':' after argument list");
  }
  // Eat ':'
  this->getNextToken();

  if (this->cur_tok != Lexer::tok_type) {
    return this->logError<PrototypeASTPtr>("Expected type specifier after ':'");
  }
  // Store return type
  bbop::Type ret_type = this->lexer->getType();

  // Eat type specifier
  this->getNextToken();

  // Fill the symbol table with the argument names
  for (auto arg : args) {
    this->symtable->variables[arg.second] = Variable(arg.first);
  }

  // Before ending this function, add the prototype to the symbol table
  this->symtable->functions[name] = Function(std::move(arg_types), ret_type);

  // Finally, create the node and return it
  return std::make_unique<PrototypeAST>(name, args, ret_type);
}

FunctionASTPtr Parser::parseDefinition() {
  // We expected this function to be called on the "fun" token.
  auto proto = this->parsePrototype();
  if (!proto) {
    return nullptr;
  }

  // parse function body
  if (auto body = this->parseInstr()) {
    return std::make_unique<FunctionAST>(std::move(proto), std::move(body));
  }

  // Clear the symbol table after parsing the function body
  this->symtable->variables.clear();

  return nullptr;
  // Now we should be after the prototype
}
