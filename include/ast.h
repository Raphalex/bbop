/** \file ast.h
 *  \brief Different classes reprensenting types of AST nodes
 */
#ifndef __AST_H__
#define __AST_H__

#include "type.h"
#include "visitor.h"

#include <memory>
#include <vector>

// Simple macro to make nodes visitable
#define MAKE_VISITABLE                                                         \
public:                                                                        \
  void accept(Visitor &visitor) override { visitor.visit(*this); }

/**
 * \brief Base AST node
 */
class AST {
public:
  // Virtual accept function for the visitor pattern
  virtual void accept(Visitor &visitor) = 0;
};

/**
 * \brief Instruction node. An instruction can be an expression or
 * another type of command (block of instructions, if / else, for, return, etc.)
 */
class InstrAST : public AST {
public:
  // Virtual destructor
  virtual ~InstrAST() {}
  InstrAST() {}
};

using InstrASTPtr = std::unique_ptr<InstrAST>;

/**
 * \brief Block node. A block contains multiple instructions
 */
class BlockAST : public InstrAST {
  MAKE_VISITABLE
private:
  // Vector of instructions
  std::vector<InstrASTPtr> instructions;

public:
  /** Constructor
   * \p instructions : vector of unique_ptr to instructions
   */
  BlockAST(std::vector<InstrASTPtr> instructions);

  /** Getter for the instruction vector
   * Warning : uses std::move
   */
  std::vector<InstrASTPtr> getInstructions();
};

/**
 * \brief Expression node. Is a parent to a lot of other nodes.
 * This class should not be instanciated.

 * An expression is an instruction
 */
class ExprAST : public InstrAST {
protected:
  /** Type of the expression */
  bbop::Type type;

public:
  // Virtual desctructor
  virtual ~ExprAST(){};

  // Getter for the type
  inline bbop::Type getType() { return this->type; }

  ExprAST(bbop::Type type);
};

/**
 * Quick shortcut to avoid typing this all the time.
 */
using ExprASTPtr = std::unique_ptr<ExprAST>;

/**
 * \brief Return instruction
 */
class ReturnAST : public InstrAST {
  MAKE_VISITABLE
private:
  // The expression to return
  ExprASTPtr expr;

public:
  /**
   * Constructor
   */
  ReturnAST(ExprASTPtr expr);

  /** Getter for the expression
   * Warning : uses std::move
   */
  ExprASTPtr getExpr();
};

class DeclAST : public InstrAST {
  MAKE_VISITABLE
private:
  // Type of the declared variables
  bbop::Type type;
  // Variable names
  std::vector<std::string> names;
  // Variable initialisation expressions
  std::vector<ExprASTPtr> init;

public:
  /**
   * Constructor
   * \p type : type of the expressions
   * \p names : names of the variables
   * \p init : ExprASTPtr vector representing the initialisation of each
   * variable
   */
  DeclAST(bbop::Type type, std::vector<std::string> names,
          std::vector<ExprASTPtr> init);

  /* Getters */

  /**
   * Getter for the type
   */
  bbop::Type getType();

  /**
   * Getter for the variable names
   * Warning : uses std::move
   */
  std::vector<std::string> getNames();

  /**
   * Getter for the variable initialisations
   * Warning : uses std::move
   */
  std::vector<ExprASTPtr> getInit();
};

class IfElseAST : public InstrAST {
  MAKE_VISITABLE
private:
  // Condition
  ExprASTPtr cond;
  // Then block
  InstrASTPtr then_instr;
  // Else block, can be nullptr
  InstrASTPtr else_instr;

public:
  /**
   * Constructor
   */
  IfElseAST(ExprASTPtr cond, InstrASTPtr then_instr, InstrASTPtr else_instr);

  /* Getters for the instructions and the condition, warning, this uses
   * std::move */
  InstrASTPtr getThen();

  InstrASTPtr getElse();

  ExprASTPtr getCond();
};

class WhileAST : public InstrAST {
  MAKE_VISITABLE
private:
  // Condition
  ExprASTPtr cond;
  // Block
  InstrASTPtr instr;

public:
  /**
   * Constructor
   * \p cond : while condition
   * \p instr : instructions to execute in the loop
   */
  WhileAST(ExprASTPtr cond, InstrASTPtr instr);

  /**
   * Getter for condition
   * Warning : uses std::move
   */
  ExprASTPtr getCond();

  /**
   * Getter for instruction(s)
   * Warning : uses std::move
   */

  InstrASTPtr getInstr();
};

/**
 * \brief Number node. Can be of type float or int.
 */
class NumberExprAST : public ExprAST {
  MAKE_VISITABLE
public:
  /**
   * Simple enum type to check this node's type.
   */

private:
  /**
   * Float value, to use only when type = float_type.
   */
  double f_value;
  /**
   * Same as f_value but for integers.
   */
  unsigned long long i_value;

public:
  /* Float constructor */
  NumberExprAST(double val);
  /* Integer constructor */
  NumberExprAST(unsigned long long val);

  /**
   * Getter for int value
   */
  unsigned long long getIntValue();

  /**
   * Getter for float value
   */

  double getFloatValue();
};

class UnaryExprAST : public ExprAST {
  MAKE_VISITABLE
private:
  /**
   * Operator
   */
  char op;

  /**
   * Operand
   */
  ExprASTPtr operand;

public:
  UnaryExprAST(char op, ExprASTPtr operand);

  /**
   * Getter for operator
   */
  char getOp();

  /**
   * Getter for the operand
   * warning : uses std::move
   */
  ExprASTPtr getOperand();
};

class BinaryExprAST : public ExprAST {
  MAKE_VISITABLE
private:
  /**
   * Operation.
   */
  int op;

public:
  /**
   * Left hand side and right hand side of the expression.
   */
  ExprASTPtr lhs, rhs;

  /** Constructor
   * \p op : int representing the operator
   * \p lhs : left hand side of expression
   * \p rhs : right hand side of expression
   * \p type : expression type
   */
  BinaryExprAST(int op, ExprASTPtr lhs, ExprASTPtr rhs, bbop::Type type);

  /**
   * Getter for the operator
   */
  int getOp();
};

class VariableExprAST : public ExprAST {
  MAKE_VISITABLE
private:
  /** Variable name */
  std::string name;

public:
  /** Constructor
   * \p name : variable name
   * \p type : variable type
   */
  VariableExprAST(const std::string &name, bbop::Type type);

  /**
   * Getter for the name of the variable
   */
  std::string getName();
};

class CallExprAST : public ExprAST {
  MAKE_VISITABLE
private:
  /** Function name */
  std::string callee;
  /** arguments to the function */
  std::vector<ExprASTPtr> args;

public:
  /** Constructor
   * \p callee : name of the function to call
   * \p args : list of expressions (arguments to the function)
   * \p type : type of this expression (effectively the function's return type)
   */
  CallExprAST(const std::string &callee, std::vector<ExprASTPtr> args,
              bbop::Type type);

  /* Getters */
  std::string getCallee();

  /**
   * Getter for the arguments
   * Warning: this uses std::move
   */
  std::vector<ExprASTPtr> getArgs();
};

/**
 * AST class for function prototypes
 */
class PrototypeAST : public AST {
  MAKE_VISITABLE
private:
  // Function name
  std::string name;
  // Argument list : pairs of type and name
  std::vector<std::pair<bbop::Type, std::string>> args;
  // Return type
  bbop::Type ret_type;

public:
  /**
   * Prototype constructor
   */
  PrototypeAST(const std::string &name,
               std::vector<std::pair<bbop::Type, std::string>> args,
               bbop::Type ret_type);

  /* Getters */
  std::string getName();

  std::vector<std::pair<bbop::Type, std::string>> getArgs();

  std::vector<bbop::Type> getArgTypes();

  std::vector<std::string> getArgNames();

  bbop::Type getRetType();
};

using PrototypeASTPtr = std::unique_ptr<PrototypeAST>;

/**
 * AST class for function definitions (proto + body)
 */

class FunctionAST : public AST {
  MAKE_VISITABLE
private:
  // Function prototype
  PrototypeASTPtr proto;

  // Function body
  InstrASTPtr body;

public:
  /**
   * Function constructor
   */
  FunctionAST(PrototypeASTPtr proto, InstrASTPtr body);

  /* Getters */

  /**
   * Getter for the function prototype.
   * Warning : proto is a unique_ptr, this getter uses std::move which means
   * the pointer will be valid only for the calling function
   */
  PrototypeASTPtr getProto();

  /**
   * Getter for the body, uses std::move
   */
  InstrASTPtr getBody();
};

using FunctionASTPtr = std::unique_ptr<FunctionAST>;

#endif