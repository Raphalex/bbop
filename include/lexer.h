/** \file lexer.h
 * \brief Everything related to the lexer.
 */

#ifndef __LEXER_H__
#define __LEXER_H__

#include "type.h"

#include <iostream>
#include <limits>
#include <memory>
#include <string>

/**
 * \brief Lexer class used to analyse tokens from input file.
 */
class Lexer {
public:
  /**
   * \brief This enum lists all of the language tokens->
   *
   * It starts from INT32_MIN so the token codes don't collide
   * with standard characters that can be returned from the gettok method.
   */
  enum Token {
    /* Numbers */
    tok_float = INT32_MIN,
    tok_int,

    /* Identifiers */
    tok_id,

    /* Keywords */
    tok_fun,
    tok_extern,
    tok_return,
    tok_if,
    tok_else,
    tok_decl,
    tok_while,

    /* Operators */
    tok_assign,
    tok_ne,
    tok_ge,
    tok_le,

    /* Type specifier */
    tok_type,

  };

private:
  /** Input from which the lexer will read. */
  std::istream &input = std::cin;

  /** Last character read from the input. */
  char last_char = ' ';

  /** If the lexer finds an identifier, the string for that identifier will be
   * stored here */
  std::string id_str;

  /** If the lexer finds a float, its value will be stored here.
   */
  double float_num_val;

  /** If the lexer finds an integer, its value will be stored here.
   */
  unsigned long long int_num_val;

  /** Actual value of type specifier if token is tok_type */
  bbop::Type type;

  /** Current line number */
  size_t line_number = 1;

  /**
   * In the case where the lexer finds an id or a keyword, this function will
   * return the corresponding token.
   *
   * id_str needs to be set
   *
   * \returns tok_id or one of the keyword tokens from the Token enum
   */
  Token getIdOrKeywordTok();

  /**
   * In the case where the lexer finds a number, this function will return
   * the corresponding token, and fill the corresponding value member.
   *
   * \p num_str : the string representing the number to parse
   *
   * \returns tok_float or tok_int
   */
  Token getNumTok(const std::string &num_str);

  /**
   * Log error
   */
  void logError(const std::string &str);

public:
  /**
   * \p input : the input to the lexer
   */
  Lexer(std::istream &input);

  /**
   * \brief Default constructor that uses stdin as the input.
   */
  Lexer(){};

  /**
   * \returns the next token in the program.
   */
  int getTok();

  /**
   * \returns the float value.
   */
  double getFloatNumValue();

  /**
   * \returns the int value.
   */
  unsigned long long getIntNumValue();

  /**
   * \returns the stored identifier string
   */
  std::string getIdStr();

  /**
   * \returns the stored type specifier
   */
  bbop::Type getType();

  /**
   * \returns current line number
   */
  size_t getLineNumber();
};

#endif