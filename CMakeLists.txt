cmake_minimum_required(VERSION 3.21)

# Set project name
enable_language(C)
set(TARGET "bbop")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "./bin")
project(${TARGET} LANGUAGES CXX)

# include directories
include_directories(include)
find_package(LLVM REQUIRED CONFIG)

include_directories(${LLVM_INCLUDE_DIRS})
add_definitions(${LLVM_DEFINITIONS})
llvm_map_components_to_libnames(llvm_libs all)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_MODULE_PATH cmake_modules)
set(CMAKE_BUILD_TYPE Debug)

add_executable(${TARGET} "")
aux_source_directory(src SOURCES)
aux_source_directory(include SOURCES)
target_link_libraries(${TARGET} LLVM)
target_sources(${TARGET} PUBLIC ${SOURCES})
