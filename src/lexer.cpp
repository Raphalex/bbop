#include "lexer.h"
#include "color.h"

#include <iostream>

Lexer::Lexer(std::istream &input) : input(input) {}

int Lexer::getTok() {
  // Ignore all white space
  while (isspace(this->last_char)) {
    if (this->last_char == '\n') {
      this->line_number++;
    }
    this->last_char = this->input.get();
  }

  /* If we find an alphabetic letter or an underscore, that
     means we found either a language keyword or an identifier */
  if (isalpha(this->last_char) || this->last_char == '_') {
    /* We loop and fill the identifier string until we need a non-alphanumeric
     * character */
    this->id_str = this->last_char;
    while (isalnum(this->last_char = this->input.get())) {
      this->id_str += this->last_char;
    }
    return this->getIdOrKeywordTok();
  }

  /* If we find a digit or a '.', this is a number.
     we need to identify its type, this is done in the get_num_tok function */
  if (isdigit(this->last_char) || this->last_char == '.') {
    std::string num_str = "";
    // Keeps track of if the '.' separator has been found
    bool separator = (this->last_char == '.');
    /* We do the same thing here like with the identifiers, loop while
       the characters are part of the number (. or digit) */

    do {
      num_str += this->last_char;
      this->last_char = this->input.get();
      if (this->last_char == '.') {
        if (separator) {
          this->logError("Unexpected '.'");
          return EOF;
        }
        separator = true;
      }
    } while (isdigit(this->last_char) || this->last_char == '.');
    return this->getNumTok(num_str);
  }

  /* Special case for each 2 characters operators */
  std::string op = "";
  op += this->last_char;
  int ret_char = this->last_char;
  this->last_char = this->input.get();
  op += this->last_char;
  if (op == ":=") {
    this->last_char = this->input.get();
    return tok_assign;
  }
  if (op == "!=") {
    this->last_char = this->input.get();
    return tok_ne;
  }
  if (op == "<=") {
    this->last_char = this->input.get();
    return tok_le;
  }
  if (op == ">=") {
    this->last_char = this->input.get();
    return tok_ge;
  }

  /* If nothing is matched, we simply return the character.*
     can be used to handle operations (+, *, ...) and ponctuation
     ({}).
     We need to do this switch because we need to advance the pointer in the
     input before the next call to get_tok. */

  return ret_char;
}

double Lexer::getFloatNumValue() { return this->float_num_val; }

unsigned long long Lexer::getIntNumValue() { return this->int_num_val; }

std::string Lexer::getIdStr() { return this->id_str; }

bbop::Type Lexer::getType() { return this->type; }

size_t Lexer::getLineNumber() { return this->line_number; }

Lexer::Token Lexer::getIdOrKeywordTok() {
  // One case for each keyword
  if (this->id_str == "fun")
    return tok_fun;
  if (this->id_str == "extern")
    return tok_extern;
  if (this->id_str == "return")
    return tok_return;
  if (this->id_str == "if")
    return tok_if;
  if (this->id_str == "else")
    return tok_else;
  if (this->id_str == "decl")
    return tok_decl;
  if (this->id_str == "while")
    return tok_while;

  /* TODO: Factor this : make a type string variable, similar to id_str, to
   * allow the parser to look up info about the type in the type table */
  // One case for each type specifier
  if (this->id_str == "i64") {
    this->type = bbop::i64;
    return tok_type;
  }
  if (this->id_str == "f64") {
    this->type = bbop::f64;
    return tok_type;
  }

  // In case no matching keyword is found, it is an identifier
  return tok_id;
}

Lexer::Token Lexer::getNumTok(const std::string &num_str) {
  // We check for a '.' to see if the number is a float or int
  std::size_t is_float = num_str.find('.');

  // float case
  if (is_float != std::string::npos) {
    this->float_num_val = std::stod(num_str);
    return tok_float;
  }
  // int case
  else {
    this->int_num_val = std::stoull(num_str);
    return tok_int;
  }
}

void Lexer::logError(const std::string &str) {
  std::cerr << color::red << "Error: " << color::reset << str << std::endl;
}
