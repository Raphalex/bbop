#include "ast.h"
#include "visitor.h"

NumberExprAST::NumberExprAST(double val) : f_value(val), ExprAST(bbop::f64) {}

NumberExprAST::NumberExprAST(unsigned long long val)
    : i_value(val), ExprAST(bbop::i64) {}

unsigned long long NumberExprAST::getIntValue() { return this->i_value; }

double NumberExprAST::getFloatValue() { return this->f_value; }

UnaryExprAST::UnaryExprAST(char op, ExprASTPtr operand)
    : op(op), ExprAST(operand->getType()), operand(std::move(operand)) {}

char UnaryExprAST::getOp() { return this->op; }

ExprASTPtr UnaryExprAST::getOperand() { return std::move(this->operand); }

BinaryExprAST::BinaryExprAST(int op, ExprASTPtr lhs, ExprASTPtr rhs,
                             bbop::Type type)
    : op(op), lhs(std::move(lhs)), rhs(std::move(rhs)), ExprAST(type) {}

int BinaryExprAST::getOp() { return this->op; }

VariableExprAST::VariableExprAST(const std::string &name, bbop::Type type)
    : name(name), ExprAST(type) {}

std::string VariableExprAST::getName() { return this->name; }

CallExprAST::CallExprAST(const std::string &callee,
                         std::vector<ExprASTPtr> args, bbop::Type type)
    : callee(callee), args(std::move(args)), ExprAST(type) {}

std::string CallExprAST::getCallee() { return this->callee; }

std::vector<ExprASTPtr> CallExprAST::getArgs() { return std::move(this->args); }

ExprAST::ExprAST(bbop::Type type) : type(type) {}

PrototypeAST::PrototypeAST(const std::string &name,
                           std::vector<std::pair<bbop::Type, std::string>> args,
                           bbop::Type ret_type)
    : name(name), args(std::move(args)), ret_type(ret_type) {}

std::string PrototypeAST::getName() { return this->name; }

std::vector<std::pair<bbop::Type, std::string>> PrototypeAST::getArgs() {
  return this->args;
}

std::vector<bbop::Type> PrototypeAST::getArgTypes() {
  std::vector<bbop::Type> types;
  for (auto arg : this->args) {
    types.push_back(arg.first);
  }
  return types;
}

std::vector<std::string> PrototypeAST::getArgNames() {
  std::vector<std::string> names;
  for (auto arg : this->args) {
    names.push_back(arg.second);
  }
  return names;
}

bbop::Type PrototypeAST::getRetType() { return this->ret_type; }

FunctionAST::FunctionAST(PrototypeASTPtr proto, InstrASTPtr body)
    : proto(std::move(proto)), body(std::move(body)) {}

PrototypeASTPtr FunctionAST::getProto() { return std::move(this->proto); }

InstrASTPtr FunctionAST::getBody() { return std::move(this->body); }

BlockAST::BlockAST(std::vector<InstrASTPtr> instructions)
    : instructions(std::move(instructions)) {}

std::vector<InstrASTPtr> BlockAST::getInstructions() {
  return std::move(this->instructions);
}

ReturnAST::ReturnAST(ExprASTPtr expr) : expr(std::move(expr)) {}

ExprASTPtr ReturnAST::getExpr() { return std::move(this->expr); }

DeclAST::DeclAST(bbop::Type type, std::vector<std::string> names,
                 std::vector<ExprASTPtr> init)
    : type(type), names(std::move(names)), init(std::move(init)) {}

bbop::Type DeclAST::getType() { return this->type; }

std::vector<std::string> DeclAST::getNames() { return std::move(this->names); }

std::vector<ExprASTPtr> DeclAST::getInit() { return std::move(this->init); }

IfElseAST::IfElseAST(ExprASTPtr cond, InstrASTPtr then_instr,
                     InstrASTPtr else_instr)
    : cond(std::move(cond)), then_instr(std::move(then_instr)),
      else_instr(std::move(else_instr)) {}

InstrASTPtr IfElseAST::getThen() { return std::move(this->then_instr); }

InstrASTPtr IfElseAST::getElse() { return std::move(this->else_instr); }

ExprASTPtr IfElseAST::getCond() { return std::move(this->cond); }

WhileAST::WhileAST(ExprASTPtr cond, InstrASTPtr instr)
    : cond(std::move(cond)), instr(std::move(instr)) {}

ExprASTPtr WhileAST::getCond() { return std::move(this->cond); }

InstrASTPtr WhileAST::getInstr() { return std::move(this->instr); }
