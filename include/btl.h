#ifndef __BTL_H__
#define __BTL_H__

#include "type.h"

#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Type.h"

/**
 * \file btl.h
 * \brief Utility functions for converting bbop constructs to llvm ones
 */

namespace btl {
/**
 * Function to convert the bbop::Type enum to LLVM Type* objects
 * \p type : the bbop type
 * \p context : the llvm context
 * \returns an LLVM Type * object corresponding to the correct bbop type
 */
llvm::Type *convert_type(bbop::Type type, llvm::LLVMContext &context);
} // namespace btl

#endif