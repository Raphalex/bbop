/**
 * \file parser.h
 * \brief Contains the parser class
 */

#ifndef __PARSER_H__
#define __PARSER_H__

#include "ast.h"
#include "codegen.h"
#include "lexer.h"
#include "symtable.h"

#include <map>
#include <memory>

class Parser {
private:
  /**
   * Current token the parser is looking at.
   */
  int cur_tok;

  /**
   * The lexer.
   */
  std::unique_ptr<Lexer> lexer;

  /**
   * The code generator.
   */
  std::unique_ptr<Codegen> codegen;

  /**
   * The symtable
   */
  std::shared_ptr<Symtable> symtable;

  /**
   * Operator precedence map.
   */
  std::map<int, int> binopPrecedence;

  /**
   * Get precedence of current token.
   * \returns -1 if the token is not an operator, otherwise, returns the
   * precedence of the current token.
   */

  int getTokPrecedence();

  /**
   * Simple function to look at the next token from the lexer.
   */
  int getNextToken();

  /**
   * Simple error function.
   */
  template <typename T> T logError(const std::string &str);

  /**
   * Simple warning log.
   */
  void logWarning(const std::string &str);

  /* Parsing functions, names are self-explanatory */

  /* Instructions */
  InstrASTPtr parseInstr();

  InstrASTPtr parseBlock();

  InstrASTPtr parseReturn();

  InstrASTPtr parseIfElse();

  InstrASTPtr parseWhile();

  InstrASTPtr parseDecl();

  /* Primary expressions */
  ExprASTPtr parsePrimary();

  /* Unary expressions */
  ExprASTPtr parseUnary();

  ExprASTPtr parseIntExpr();
  ExprASTPtr parseFloatExpr();
  ExprASTPtr parseParenExpr();

  /* Binary expressions */
  ExprASTPtr parseBinOpRHS(int expr_prec, ExprASTPtr lhs);

  /* General expressions (regroups previous methods) */
  ExprASTPtr parseExpr();

  /* Identifier expressions (function calls and variable references) */
  ExprASTPtr parseIdExpr();

  /* Functions */

  PrototypeASTPtr parsePrototype();

  FunctionASTPtr parseDefinition();

public:
  /**
   * Constructor for the parser, the lexer and code generator need to be
   * provided. \p lexer : the lexer
   * \p symtable : shared pointer on the symbol table
   * \p codegen : unique pointer on a code generation object
   */
  Parser(std::unique_ptr<Lexer> lexer, std::shared_ptr<Symtable> symtable,
         std::unique_ptr<Codegen> codegen);

  /**
   * Driver for the parser. This function issues the right calls to all the
   * areas of the parser
   */

  void mainLoop();
};

#endif