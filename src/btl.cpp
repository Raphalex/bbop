#include "btl.h"

namespace btl {
llvm::Type *convert_type(bbop::Type type, llvm::LLVMContext &context) {
  switch (type) {
  case bbop::i64:
    return llvm::Type::getInt64Ty(context);
  case bbop::f64:
    return llvm::Type::getDoubleTy(context);
  default:
    return nullptr;
  }
}
} // namespace btl