#ifndef __TYPE_H__
#define __TYPE_H__

#include <string>

namespace bbop {
enum Type { f64, i64 };

inline std::string type2str(Type type) {
  switch (type) {
  case f64:
    return "f64";
  case i64:
    return "i64";
  default:
    return "???";
  }
}
} // namespace bbop

#endif